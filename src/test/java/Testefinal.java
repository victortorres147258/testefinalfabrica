import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Testefinal {

private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver","C:/UNIPE2022/UNIPE2022/src/test/resources/firefox/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");


    }
    @After
    public void sair() {
        driver.quit();
    }

    @Test
public void TestarCadastro() throws InterruptedException {
driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[1]/td/input")).sendKeys("Cachorrinho");
Thread.sleep(3000);
driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[2]/td/input")).sendKeys("cachorrinho1");
Thread.sleep(3000);
driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[9]/td/input[2]")).click();
Thread.sleep(3000);
        Assert.assertEquals("Cachorrinho", driver.findElement(By.xpath("//*[@id=\"_valueusername\"]")).getText());
        Thread.sleep(3000);
        Assert.assertEquals("cachorrinho1", driver.findElement(By.xpath("//*[@id=\"_valuepassword\"]")).getText());
        Thread.sleep(3000);
        Assert.assertEquals("Comments...", driver.findElement(By.xpath("//*[@id=\"_valuecomments\"]")).getText());
        Thread.sleep(3000);
        Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes0")).getText());
        Thread.sleep(3000);
        Assert.assertEquals("rd2", driver.findElement(By.id("_valueradioval")).getText());
        Thread.sleep(3000);
        Assert.assertEquals("ms4", driver.findElement(By.id("_valuemultipleselect0")).getText());
        Thread.sleep(3000);
        Assert.assertEquals("dd3", driver.findElement(By.id("_valuedropdown")).getText());
        Thread.sleep(3000);

    }


}
